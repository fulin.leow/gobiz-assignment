import React from 'react';
import style from './Score.module.css';

const Score = ({ score }) => {
    return (
        <div className={style.score}>
            Score: {score}
        </div>
    );
};

export default Score;