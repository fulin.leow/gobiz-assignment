import React from 'react';
import style from './Tile.module.css';

const Tile = ({ letter }) => {
  return (
    <div className={style.tile}>
      {letter}
    </div>
  );
};

export default Tile;