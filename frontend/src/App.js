import React, { useState, useEffect, useCallback } from 'react';
import Tile from './components/Tile/Tile';
import Score from './components/Score/Score';
import Button from './components/Button/Button';
import './App.css';

// Scoring table based on the image provided
const scoringTable = {
  A: 1, E: 1, I: 1, O: 1, U: 1, L: 1, N: 1, S: 1, T: 1, R: 1,
  D: 2, G: 2,
  B: 3, C: 3, M: 3, P: 3,
  F: 4, H: 4, V: 4, W: 4, Y: 4,
  K: 6,
  J: 8, X: 8,
  Q: 10, Z: 10
};

const calculateScore = (tiles) => {
  return tiles.reduce((totalScore, tile) => {
    const tileScore = scoringTable[tile.letter.toUpperCase()] || 0;
    return totalScore + tileScore;
  }, 0);
};


const App = () => {

  const initialState = () => {
    const maxTiles = 10; //
    return Array.from({ length: maxTiles }, (_, index) => ({ letter: '', id: index }));
  };
  const [currentIndex, setCurrentIndex] = useState(0);
  const [tiles, setTiles] = useState(initialState());
  const [score, setScore] = useState(0);

  useEffect(() => {
    const handleKeyPress = (event) => {
      // Only allow alphabet characters
      if (event.key && event.key.match(/^[A-Za-z]$/) && currentIndex < 10) {
        setTiles((tiles) => {
          const newTiles = tiles.map((tile, index) =>
            index === currentIndex
              ? { ...tile, letter: event.key.toUpperCase() }
              : tile
          );
          return newTiles;
        });
        setCurrentIndex((currentIndex) => currentIndex + 1);

      } else if (["Backspace", "Delete"].includes(event.key) && currentIndex !== 0) {

        setTiles((tiles) => {
          const newTiles = tiles.map((tile, index) =>
            index === currentIndex - 1
              ? { ...tile, letter: '' }
              : tile
          );
          return newTiles;
        });
        setCurrentIndex((currentIndex) => currentIndex - 1);
      }
    };

    window.addEventListener('keydown', handleKeyPress);

    return () => window.removeEventListener('keydown', handleKeyPress);
  }, [currentIndex]);

  useEffect(() => {
    setScore(calculateScore(tiles));
  }, [tiles]);

  const handleReset = useCallback((event) => {
    setTiles(() => initialState());
    setCurrentIndex(() => 0);
    event.target.blur()
  },[]);

  const handleSaveScore = () => {
    // logic to save score
  };

  const handleViewTopScores = () => {
    // logic to view top scores
  };

  return (
    <div className="app">
      <div className="app-container">
        <div className="tiles">
          {tiles.map(tile => (
            <Tile key={tile.id} letter={tile.letter} />
          ))}
        </div>
        <Score score={score} />
        <div className="buttons">
          <Button onClick={(event) => handleReset(event)}>Reset Tiles</Button>
          <Button onClick={() => {/* logic to save score */ }}>Save Score</Button>
          <Button onClick={() => {/* logic to view top scores */ }}>View Top Scores</Button>
        </div>
      </div>
    </div>
  );
};

export default App;